import RPi.GPIO as GPIO
import time
from grove_relay import GroveRelay

pin_relay = 17

relay = GroveRelay(16)
relay1 = GroveRelay(17)

relay.off()
relay1.off()

servoPIN = 21            
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN,GPIO.OUT)

servoPIN0 = 18                
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN0,GPIO.OUT)

servoPIN1 = 11
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN1,GPIO.OUT)

p = GPIO.PWM(servoPIN, 50) # GPIO 17 ALS PWM mit 50Hz
p.start(2.5) # Initialisierung

p0 = GPIO.PWM(servoPIN0, 50) # GPIO 17 ALS PWM mit 50Hz
p0.start(2.5) # Initialisierung

p1 = GPIO.PWM(servoPIN1, 50) # GPIO 17 ALS PWM mit 50Hz
p1.start(2.5) # Initialisierung
a = 2
b = [5,1,8,3,12,9,2,4,7,6,11,10]
try:
    
    while a < 12:
#         angle = input('angle:')
        print(b[a])
        p.ChangeDutyCycle(float(b[a]))
        p0.ChangeDutyCycle(float(b[a]))
        p1.ChangeDutyCycle(float(b[a])) # float(input("angle1(2:-90grd;12.5:+90grd):"))
        a = a + 1
        time.sleep(1)
        
        #p.ChangeDutyCycle(12.5)
        #time.sleep(1)
        #p.ChangeDutyCycle(2.5)
        #time.sleep(1)
except KeyboardInterrupt:
    p.stop()
    p0.stop()
    p1.stop()
    GPIO.cleanup()
finally:
    print('clean up')
    GPIO.cleanup()