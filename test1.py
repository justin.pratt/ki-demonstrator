from datetime import datetime
import time

vBand = 10

def pos_Block(ini_time):
    dif = datetime.now() - ini_time
    posi = vBand*(dif.seconds) # aktuelle Position in mm
    if posi > 825: # 825 mm ist die Länge des Förderbands, alles dahinter bedeutet: nicht mehr auf dem Förderband -> nan
        posi = float('nan')
    return posi

initime = datetime.now()

time.sleep(3)

print(pos_Block(initime))
