# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 11:01:02 2022

@author: Justin
"""

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow import keras
from matplotlib import image
import PIL as pil
from PIL import Image
import os

#%%

class colors:
    def __init__(self,folder,colour):
        self.folder = folder
        self.colour = colour
        
    def do_everything(self):
        
        os.chdir(self.folder)
        
        onlyfiles = [f for f in os.listdir(self.folder) if os.path.exists(os.path.join(self.folder, f)) and os.path.isfile(os.path.join(self.folder, f))]
        
        color = [Image.open(i).resize((256,256)) for i in onlyfiles]
                
        color = np.array([np.array(i) for i in color])
        
        label = [self.colour for x in range(color.shape[0])]
        
        # label[:] = self.colour
        
        return color,np.array(label)
    
#%% import images

folder_blue = 'C:/Users/Justin/Git_geteilt/images/blue'
folder_red = 'C:/Users/Justin/Git_geteilt/images/red'
folder_yellow = 'C:/Users/Justin/Git_geteilt/images/yellow'

blue = colors(folder_blue,0).do_everything()
red = colors(folder_red,1).do_everything()
yellow = colors(folder_yellow,2).do_everything()

#%% preprocessing

list_colors_im = blue[0],red[0],yellow[0] #,blue_d[0],red_d[0],yellow_d[0]

list_colors_la = blue[1],red[1],yellow[1] #,blue_d[1],red_d[1],yellow_d[1]

images = np.concatenate(list_colors_im,axis=0)

labels = np.concatenate(list_colors_la,axis=0)

labels = labels.reshape(labels.shape[0])

shuffler = np.random.permutation(len(labels))

images = images[shuffler]

labels = labels[shuffler]

images = images.astype('float32')

images /= 255

train_images = images[:1000]
train_labels = labels[:1000]

test_images = images[1000:]
test_labels = labels[1000:]

#%% modelling

model = keras.Sequential([
   keras.layers.AveragePooling2D(6,3, input_shape=(256,256,3)),
   keras.layers.Conv2D(64, 3, activation='relu'),
   keras.layers.Conv2D(32, 3, activation='relu'),
   keras.layers.MaxPool2D(2,2),
   keras.layers.Dropout(0.5),
   keras.layers.Flatten(),
   keras.layers.Dense(128, activation='relu'),
   keras.layers.Dense(3, activation='sigmoid')
])

model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(),
              metrics=['accuracy'])

model.fit(train_images, train_labels, epochs=5, batch_size=32)

#%% evaluate

model.evaluate(test_images, test_labels)

#%% testing

predictions = model.predict(test_images)
# print(predictions)

#%% save model

model.save('C:/Users/Justin/Git_geteilt/colormodel')

#%% load model

model = keras.models.load_model('C:/Users/Justin/OneDrive - stud.tu-darmstadt.de/HiWi/KI-Demonstrator/Image-Classifier-main/Image-Classifier-main/k-NN_Image_Classification/imageclassifier/colormodel')

#%% Load into TFLite-model

model_direct = 'C:/Users/Justin/Git_geteilt'
tflite_model_name = 'block_model_color'

converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()
open('/'.join((model_direct, tflite_model_name)) + '.h5', 'wb').write(tflite_model)

#%% Define function to perform prediction of TFLite-model

# model_filepath = '/'.join((model_direct, tflite_model_name)) + '.h5'

def get_pred(image,model_filepath):
    image = np.asarray(image).astype(np.float32)/255
    int =  tf.lite.Interpreter(model_path=model_filepath)
    int.allocate_tensors()
    input_details = int.get_input_details()
    output_details = int.get_output_details()
    input_data = np.array(image, dtype=np.float32)
    int.set_tensor(input_details[0]['index'], input_data.reshape(1,256,256,3))
    int.invoke()
    output_data = int.get_tensor(output_details[0]['index'])
    return output_data

#%% more testing

directory = 'C:/Users/Justin/Git_geteilt/images/Fehler'

filelist = os.listdir(directory)

def test_pred(index, filelist):
    file = filelist[index]
    image = Image.open('/'.join((directory, file)))
    image = image.resize((256, 256))

    image.show()
    
    model_filepath = '/'.join((model_direct, tflite_model_name)) + '.h5'

    pred = get_pred(image,model_filepath)

    pred = pred.reshape(pred.shape[1])

    ind_pred = list(pred).index(max(pred))

    if ind_pred == 0:
        col_pred = "blue"
    elif ind_pred == 1:
        col_pred = "red"
    else:
        col_pred = "yellow"

    print(col_pred, pred)
    
test_pred(1, filelist)
    
#%% delete files

file_path = 'D:/OneDrive_justinpratt99/TUD/HiWi/PTW-qualityprediction/Image-Classifier-main/Image-Classifier-main/k-NN_Image_Classification/imageclassifier/dataset/blocks/yellow/red_array.npy'

if os.path.isfile(file_path):
  os.remove(file_path)
  print("File has been deleted")
else:
  print("File does not exist")




