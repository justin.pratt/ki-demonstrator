# -*- coding: utf-8 -*-
"""
Created on Thu Aug  4 16:24:05 2022

@author: Justin
"""

import numpy as np
import time
from datetime import datetime
from threading import Thread
from grove_ultrasonic_ranger import GroveUltrasonicRanger
import RPi.GPIO as GPIO
import cv2
from PIL import Image
from tflite_runtime.interpreter import Interpreter
from opcua import Server
from random import randint

server = Server()

url = "opc.tcp://192.168.0.161:4800"
server.set_endpoint(url)

name = "OPCUA_SIMULATION_SERVER"
addspace = server.register_namespace(name)

node = server.get_objects_node()

Param = node.add_object(addspace, "Parameters")

star = Param.add_variable(addspace, "Start", True) # start of conveyer belt

star.set_writable()

server.start()

print("Server started at {}".format(url))

sta = True
while True:
    if sta == True:
        sta = False
        star.set_value(sta)
        time.sleep(2)
    else:
        sta = True
        star.set_value(sta)
        time.sleep(2)
    print(sta)
        


cam = cv2.VideoCapture(0)
    
# cv2.namedWindow("test")

# insert k = cv2.waitKey(1) somewhere in Loop, e.g. after try:

# image = Image.open('C:/Users/Justin/Git_geteilt/images/opencv_frame_7.png')

# image = image.resize((256, 256))

# print(image.size)

def color_detector(image,model_filepath):
    int =  Interpreter(model_path=model_filepath)
    int.allocate_tensors()
    input_details = int.get_input_details()
    output_details = int.get_output_details()
    input_data = np.array(image, dtype=np.float32)
    int.set_tensor(input_details[0]['index'], input_data.reshape(1,256,256,3))
    int.invoke()
    output_data = int.get_tensor(output_details[0]['index'])
    return output_data

def defect_detector(image):
    models_path_dec = '/home/pi/Desktop/gitGeteilt/ki-demonstrator'
    tflite_model_name_dec = 'block_model_dec200'          # Will be given .tflite suffix
    
    models_path_enc = '/home/pi/Desktop/gitGeteilt/ki-demonstrator'
    tflite_model_name_enc = 'block_model_enc200'          # Will be given .tflite suffix

#     image = Image.open(filePath)
#     image = image.resize((256, 256))
#     image = np.asarray(image).astype(np.float32)/255

    
    TFLITE_FILE_PATH_enc = '/'.join((models_path_enc, tflite_model_name_enc)) + '.h5'
    enc_int =  Interpreter(model_path=TFLITE_FILE_PATH_enc)
    enc_int.allocate_tensors()
    input_details_enc = enc_int.get_input_details()
    output_details_enc = enc_int.get_output_details()
    input_data_enc = np.array(image, dtype=np.float32)
    enc_int.set_tensor(input_details_enc[0]['index'], input_data_enc.reshape(1,256,256,3))
    enc_int.invoke()
    output_data_enc = enc_int.get_tensor(output_details_enc[0]['index'])

    TFLITE_FILE_PATH_dec = '/'.join((models_path_dec, tflite_model_name_dec)) + '.h5'
    dec_int =  Interpreter(model_path=TFLITE_FILE_PATH_dec)
    dec_int.allocate_tensors()
    input_details_dec = dec_int.get_input_details()
    output_details_dec = dec_int.get_output_details()
    input_data_dec = np.array(output_data_enc, dtype=np.float32)
    dec_int.set_tensor(input_details_dec[0]['index'], input_data_dec)
    dec_int.invoke()
    output_data = dec_int.get_tensor(output_details_dec[0]['index'])

    # output_data1 = output_data[0,:,:,:]
    # Image.fromarray(np.uint8(output_data1*255)).show()
    
    dect_image = image

    dect_gen_image = np.array(output_data[0,:,:,:])

    loss = np.mean(np.square(dect_image-dect_gen_image))
    
    threash = 0.00126
    
    predi = loss < threash
    
    return predi
    



servoPIN1 = 11
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN1,GPIO.OUT)
p1 = GPIO.PWM(servoPIN1,50)
p1.start(2.5)

servoPIN2 = 18
GPIO.setup(servoPIN2,GPIO.OUT)
p2 = GPIO.PWM(servoPIN2,50)
p2.start(2.5)

servoPIN3 = 21
GPIO.setup(servoPIN3,GPIO.OUT)
p3 = GPIO.PWM(servoPIN3,50)
p3.start(2.5)

RangerClass = GroveUltrasonicRanger(5)

#%%

block_model_filepath = '/home/pi/Desktop/gitGeteilt/ki-demonstrator/pseudo_code/block_model_color.h5'

vBand = 50 # Bandgeschwindigkeit in mm/s
# sBlock = RangerClass._get_distance() # Blockentfernung von Ultraschallsensor in mm
# pred = np.array([0,1]) # Stelle 0: "0"=blue, "1"=red, "2"=yellow; Stelle 1: "0" = kein Defekt, "1"=Defekt
all_blocks = range(1000000) # Gesamtanzahl der Blöcke
a = list(np.zeros(max(all_blocks)))
b = list(np.zeros(max(all_blocks)))
pos = list(np.zeros(max(all_blocks))) # Initialisierung: Zeit, in der alle Blöcke das erste Mal vom Ultraschall erkannt werden
pos[0] = datetime.now()

p1.ChangeDutyCycle(float(2.5)) # Aktivierung von Servomotor 1 (vorne); True: ausgefahren/aktiviert, False: eingfahren/deaktiviert
p2.ChangeDutyCycle(float(2.5)) # Aktivierung von Servomotor 2 (mitte)
# p3.ChangeDutyCycle(float(2.5)) # Aktivierung von Servomotor 3 (hinten)


def pos_Block(ini_time,vBand=50):
    dif = datetime.now() - ini_time
    posi = vBand*(dif.seconds) # aktuelle Position in mm
    if posi > 825: # 825 mm ist die Länge des Förderbands, alles dahinter bedeutet: nicht mehr auf dem Förderband -> nan
        posi = float('nan')
    return posi



class demonstrator():
    def __init__(self,vBand):
        # self.pred = pred
        self.i = 0
        self.vBand = vBand
        self.block_model_filepath = '/home/pi/Desktop/gitGeteilt/ki-demonstrator/block_model_color.h5'

    def ask_pred(self):
        try:
            sBlock = float(RangerClass.get_distance())
            while sBlock > 105:
                sBlock = float(RangerClass.get_distance())
            # pred0 = float(input('Vorhersage Farbe (0:b,1:r,2:y): '))
            ret, frame = cam.read()
            # cv2.imshow('test', frame)
            # Image.fromarray(frame).show()
            k = cv2.waitKey(1)
            frame = cv2.resize(frame, (256,256))
            evalue0 = max(color_detector(frame, self.block_model_filepath))
            print(evalue0)
            pred0 = list(evalue0).index(max(evalue0))
            print(pred0)
            pred1 = defect_detector(frame)
#             pred1 = float(input('Vorhersage Zustand (0:gut,1:schlecht): '))
            self.pred = np.array([pred0,pred1])
            print(self.pred)
            # a[self.i] = Thread(target = demonstrator.sorting, args = (self,))
            # a[self.i].start()
            
            # ab hier ehemaliges "def sorting(self)"
            print(self.i)
            time.sleep(2)
            self.i += 1
            ini_time = datetime.now()
            # pos[self.i] = pos_Block(ini_time)
            pos[self.i] = ini_time
            if self.pred[1] == 1:
                b[self.i] = Thread(target = demonstrator.ask_pred, args = (self,))
                b[self.i].start()
                p1.ChangeDutyCycle(float(2.5)) #2.5, falls deaktiviert, 5, falls aktiviert
                p2.ChangeDutyCycle(float(2.5)) # alle drei Servos bleiben deaktiviert, dass der Block hinten rausfallen kann
                p3.ChangeDutyCycle(float(2.5))
            elif self.pred[1] == 0:
                if self.pred[0] == 0:
                    print(self)
                    b[self.i] = Thread(target = demonstrator.ask_pred, args = (self,))
                    b[self.i].start()
                    print(pos[self.i-1])
                    while (pos_Block(pos[self.i-1]) < 235) or not np.isnan(pos_Block(pos[self.i-1])): # Schleife verhindert, dass der Servo aktiviert wird bevor der vorherige Block vorbeigefahren ist
                        time.sleep(0.1)
                    p1.ChangeDutyCycle(float(5)) # Block wird in Loch 1 sortiert
                    p2.ChangeDutyCycle(float(2.5))
                    p3.ChangeDutyCycle(float(2.5))
                    time.sleep(235/self.vBand) # 235 mm gefahren (zum Ende des erste Lochs), ab Ultraschall, bis Ende Loch
                    p1.ChangeDutyCycle(float(2.5))
                elif self.pred[0] == 1:
                    b[self.i] = Thread(target = demonstrator.ask_pred, args = (self,))
                    b[self.i].start()
                    while (pos_Block(pos[self.i-1]) < 385) or not np.isnan(pos_Block(pos[self.i-1])):
                        time.sleep(0.1)
                    p1.ChangeDutyCycle(float(2.5))
                    p2.ChangeDutyCycle(float(5)) # Block wird in Loch 2 sortiert
                    p3.ChangeDutyCycle(float(2.5))
                    time.sleep(385/self.vBand) # 385 mm gefahren (zum Ende des zweiten Lochs)
                    p2.ChangeDutyCycle(float(2.5))
                elif self.pred[0] == 2:
                    b[self.i] = Thread(target = demonstrator.ask_pred, args = (self,))
                    b[self.i].start()
                    while (pos_Block(pos[self.i-1]) < 535) or not np.isnan(pos_Block(pos[self.i-1])):
                        time.sleep(0.1)
                    p1.ChangeDutyCycle(float(2.5))
                    p2.ChangeDutyCycle(float(2.5))
                    p3.ChangeDutyCycle(float(5)) # Block wird in Loch 3 sortiert
                    time.sleep(535/self.vBand) # 535 mm gefahren (zum Ende des dritten Lochs)
                    p3.ChangeDutyCycle(float(5))
        except KeyboardInterrupt:
            p1.stop()
            p2.stop()
            p3.stop()
            GPIO.cleanup()
            print("exit")
            exit(1)  
        
if __name__ == '__main__':
    i = 0
    dem = demonstrator(vBand)
    dem.ask_pred()
        # a[j] = Thread(target = demonstrator.sorting, args = (sBlock,pred,j,i,vBand))
        # b[j] = Thread(target = demonstrator.sorting, args = (sBlock,pred,j,i,vBand))
        # pred0 = a[j].start() # k = i(in Funktion)
            
            
cam.release()

cv2.destroyAllWindows()

#%% test 1

# for k in np.linspace(1,5,5):
#     time.sleep(1)
#     print(f'{k}s passed so far')
            
# #%% test 3
# 
# num5 = input('number 5:')
# 
# print(num5)
# 
# #%% test 4
# 
# import threading
# import queue
# 
# q = queue.Queue()
# 
# def worker():
#     while True:
#         item = q.get()
#         print(f'Working on {item}')
#         print(f'Finished {item}')
#         q.task_done()
# 
# # Turn-on the worker thread.
# threading.Thread(target=worker, daemon=True).start()
# 
# # Send thirty task requests to the worker.
# for item in range(30):
#     q.put(item)
# 
# # Block until all tasks are done.
# q.join()
# print('All work completed')
# 
# #%% test 5
# 
# from threading import Thread
# 
# def func1():
#     print('worker 1')
# 
# def func2():
#     print("worker 2")
# 
# if __name__ == '__main__':
#     a = Thread(target = func1)
#     b = Thread(target = func1)
#     a.start()
#     b.start()
#     
# #%% test 6
# 
# def test(something,h):
#     print(something)
#     summe = h+2
#     print(summe)
#     return summe
# 
# a = list(np.zeros(5))
# 
# if __name__ == '__main__':
#     for something in range(5):
#         h = 2
#         a[something] = Thread(target = test,args=(something,h))
#         a[something].start()
#     
# #%% test 7
# 
# now = time.time()
# 
# print('10s start now')
# 
# def wait_until(somepredicate, timeout, period=0.25, *args, **kwargs):
#   mustend = time.time() + timeout
#   while time.time() < mustend:
#     if somepredicate(*args, **kwargs): return True
#     time.sleep(period)
#   return False
# 
# wait_until((time.time()-now)>10,20)
# 
# print('it waited 10s')