# -*- coding: utf-8 -*-
"""
Created on Wed Aug 31 10:25:54 2022

@author: Justin
"""

import os
import numpy as np
import pandas as pd

def get_info(path,color,cond):
    files = [os.path.splitext(filename)[0] for filename in os.listdir(path)]
    # print(files)
    color_info = np.array([color] * len(files))
    
    cond_info = np.array([cond] * len(files))
    
    # infos = [[files],[color_info],[cond_info]]
    
    infos = np.array([files,color_info,cond_info])
    
    return infos

opath = 'C:/Users/Justin/Git_geteilt/data_new/'

path = opath + 'blue'

infos_blue = get_info(path,'blue','good')

path = path = opath + 'blue_defect'

infos_blue_d = get_info(path,'blue','defect')

path = path = opath + 'red'

infos_red = get_info(path,'red','good')

path = path = opath + 'red_defect'

infos_red_d = get_info(path,'red','defect')

path = path = opath + 'yellow'

infos_yellow = get_info(path,'yellow','good')

path = path = opath + 'yellow_defect'

infos_yellow_d = get_info(path,'yellow','defect')

infos = np.concatenate((infos_blue,infos_blue_d,infos_red,infos_red_d,infos_yellow,infos_yellow_d),axis=1).transpose()

#%%

os.chdir(opath)

infos_df = pd.DataFrame(infos)

infos_df.columns = ['filename','color','condition']

infos_df.to_csv('infos.csv',index=False)


#%% remove files that are mistakenly in blue folder

path = 'C:/Users/Justin/Git_geteilt/Pytorch_Model/data/blue'

files = os.listdir(path)

# print(files[0].contains('bd'))

# print('bd' in files[1])

for i in files:
    if 'bd' in i:
        os.remove(path + '/' + i)
    
    