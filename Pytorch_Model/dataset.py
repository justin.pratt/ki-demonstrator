# -*- coding: utf-8 -*-
"""
Created on Wed Aug 31 10:04:50 2022

@author: Justin
"""

mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]

import csv

import numpy as np
from PIL import Image
from torch.utils.data import Dataset

class AttributesDataset():
    def __init__(self, annotation_path):
        color_labels = []
        condition_labels = []

        with open(annotation_path) as f:
            reader = csv.DictReader(f)
            for row in reader:
                color_labels.append(row['color'])
                condition_labels.append(row['condition'])

        self.color_labels = np.unique(color_labels)
        self.condition_labels = np.unique(condition_labels)

        self.num_colors = len(self.color_labels)
        self.num_conditions = len(self.condition_labels)

        self.color_id_to_name = dict(zip(range(len(self.color_labels)), self.color_labels))
        self.color_name_to_id = dict(zip(self.color_labels, range(len(self.color_labels))))

        self.condition_id_to_name = dict(zip(range(len(self.condition_labels)), self.condition_labels))
        self.condition_name_to_id = dict(zip(self.condition_labels, range(len(self.condition_labels))))


class FashionDataset(Dataset):
    def __init__(self, annotation_path, attributes, transform=None):
        super().__init__()

        self.transform = transform
        self.attr = attributes

        # initialize the arrays to store the ground truth labels and paths to the images
        self.data = []
        self.color_labels = []
        self.condition_labels = []

        # read the annotations from the CSV file
        with open(annotation_path) as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.data.append(row['image_path'])
                self.color_labels.append(self.attr.color_name_to_id[row['color']])
                self.condition_labels.append(self.attr.condition_name_to_id[row['condition']])

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        # take the data sample by its index
        img_path = self.data[idx]

        # read image
        img = Image.open(img_path)

        # apply the image augmentations if needed
        if self.transform:
            img = self.transform(img)

        # return the image and all the associated labels
        dict_data = {
            'img': img,
            'labels': {
                'color_labels': self.color_labels[idx],
                'condition_labels': self.condition_labels[idx],
            }
        }
        return dict_data