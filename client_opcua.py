
import opcua
from opcua import Client

serv = Client("opc.tcp://192.168.138.248:4840")

serv.connect()

serv.get_namespace_array()

objects = serv.get_objects_node()

start = objects.get_children()[0].get_children()[0].get_children()[0].get_children()[0].get_children()[8].get_children()[0].get_children()[0]
stop = objects.get_children()[0].get_children()[0].get_children()[0].get_children()[0].get_children()[8].get_children()[0].get_children()[1]

print(f"before: {start.get_value()}")
print(f"before: {stop.get_value()}")

start.set_value(False)
stop.set_value(True)

print(f"after: {start.get_value()}")
print(f"after: {stop.get_value()}")

