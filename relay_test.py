import time
import grovepi

# Connect the Grove SPDT Relay to digital port D4
# # SIG,NC,VCC,GND
# relay = 16
# 
# grovepi.pinMode(relay,"OUTPUT")
# 
# while True:
#     try:
#         # switch on for 5 seconds
#         grovepi.digitalWrite(relay,1)
#         print ("on")
#         time.sleep(5)
# 
#         # switch off for 5 seconds
#         grovepi.digitalWrite(relay,0)
#         print ("off")
#         time.sleep(5)
# 
#     except KeyboardInterrupt:
#         grovepi.digitalWrite(relay,0)
#         break
#     except IOError:
#         print ("Error")
#         
# import time
# import grovepi

# Connect the Grove 2-Coil Latching Relay to digital port D4
# SIG,NC,VCC,GND
relay = 16

grovepi.pinMode(relay,"OUTPUT")

while True:
    try:
        # switch on for 5 seconds
        grovepi.digitalWrite(relay,1)
        print ("on")
        time.sleep(5)

        # switch off for 5 seconds
        grovepi.digitalWrite(relay,0)
        print ("off")
        time.sleep(5)

    except KeyboardInterrupt:
        grovepi.digitalWrite(relay,0)
        break
    except IOError:
        print ("Error")