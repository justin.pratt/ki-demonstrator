import pickle

# obj0, obj1, obj2 are created here...
obj0 = 1
obj1 = 2
obj2 = 3

# Saving the objects:
with open('objs.pkl', 'wb') as f:  # Python 3: open(..., 'wb')
    pickle.dump([obj0, obj1, obj2], f)

#%%

# Getting back the objects:
with open('objs.pkl', 'rb') as f:  # Python 3: open(..., 'rb')
    obj0, obj1, obj2 = pickle.load(f)