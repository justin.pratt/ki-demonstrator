import pickle
import tarfile
import os
import tensorflow
import matplotlib
os.chdir('/home/pi/Desktop/gitGeteilt/ki-demonstrator/Pytorch_Anomalie_Detection/Autoencoder-in-TensorFlow')
# open a .spydata file
filename = 'right_blocks_model.spydata'
tar = tarfile.open(filename, "r")
# extract all pickled files to the current working directory
tar.extractall()
extracted_files = tar.getnames()
for f in extracted_files:
    if f.endswith('.pickle'):
         with open(f, 'rb') as fdesc:
             data = pickle.loads(fdesc.read())
# or use the spyder function directly:
from spyderlib.utils.iofuncs import load_dictionary
data_dict = load_dictionary(filename)